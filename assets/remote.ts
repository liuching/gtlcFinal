// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
declare var firebase: any;
const { ccclass, property } = cc._decorator;

@ccclass
export default class remote extends cc.Component {
	//* Persist Node to store any firebase database on.

	//* check if any database active when scene changes.
	// LIFE-CYCLE CALLBACKS:

	public src = [];

	onLoad() {
		cc.game.addPersistRootNode(this.node);
	}

	add(ref, type, func) {
		this.src.push({ ref: ref, type: type, func: func });
	}

	clear() {
		let database = firebase.database();
		for (let i = 0; i < this.src.length; i++) {
			database.ref(this.src[i].ref).off(this.src[i].type, this.src[i].func);
		}
	}

	start() {}

	// update (dt) {}
}
