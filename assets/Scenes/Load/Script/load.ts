const { ccclass, property } = cc._decorator;

@ccclass
export default class load extends cc.Component {
	start() {
		let ret = cc.callFunc(() => {
			cc.director.loadScene('Game');
		});

		cc.find('knight').runAction(cc.sequence(cc.moveTo(22, cc.v2(930, 70)), cc.delayTime(2), ret));
		this.scheduleOnce(() => {
			cc.find('Canvas/background/operation').active = false;
			cc.find('Canvas/background/operation2').active = true;
		}, 7);
		this.scheduleOnce(() => {
			cc.find('Canvas/background/operation2').active = false;
			cc.find('Canvas/background/operation3').active = true;
		}, 16);
	}
}
