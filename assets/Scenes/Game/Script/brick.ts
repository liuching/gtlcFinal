// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class brick extends cc.Component {
	// LIFE-CYCLE CALLBACKS:

	@property(cc.Node)
	engineer: cc.Node = null;

	public local;

	//* status
	public NodeYPrev;
	public nailed = false;

	//* Control.
	public LKey = cc.macro.KEY.a;
	public RKey = cc.macro.KEY.d;
	public DownKey = cc.macro.KEY.s;
	public LRotateKey = cc.macro.KEY.q;
	public RRotateKey = cc.macro.KEY.e;

	public leftDown: boolean = false;
	public rightDown: boolean = false;
	public zDown: boolean = false;
	public xDown: boolean = false;
	public spaceDown: boolean = false;

	//* trigger
	public zPrev: boolean = false;
	public xPrev: boolean = false;

	public Speed: number;

	public control;
	public create;

	public finishCur = () => {
		if (this.create) {
			this.control = false;
			this.leftDown = false;
			this.rightDown = false;
			cc.find('Canvas').getComponent('engineer').createBrick();

			this.create = false;
		}
	};

	onLoad() {
		cc.director.getPhysicsManager().enabled = true;
		cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
		cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
		this.node.on(cc.Node.EventType.MOUSE_DOWN, this.nail, this);

		this.engineer = cc.find('Canvas');
		this.local = cc.find('Canvas').getComponent('connect').local;
	}

	onKeyDown(event) {
		if (this.local) {
			if (this.control) {
				if (event.keyCode == this.LRotateKey) {
					this.zDown = true;
				} else if (event.keyCode == this.RRotateKey) {
					this.xDown = true;
				} else if (event.keyCode == this.DownKey) {
					this.spaceDown = true;
				} else if (event.keyCode == this.LKey) {
					/* else if (event.keyCode == cc.macro.KEY.shift) {} */
					this.leftDown = true;
				} else if (event.keyCode == this.RKey) {
					this.rightDown = true;
				}
			}
		}
	}

	//控制方塊
	onKeyUp(event) {
		if (this.local) {
			if (event.keyCode == this.LRotateKey) {
				this.zDown = false;
			} else if (event.keyCode == this.RRotateKey) {
				this.xDown = false;
			} else if (event.keyCode == this.DownKey) {
				this.spaceDown = false;
			} else if (event.keyCode == this.LKey) {
				this.leftDown = false;
			} else if (event.keyCode == this.RKey) {
				this.rightDown = false;
			}
		}
	}

	start() {}

	zPress() {
		this.leftRotation();
	}

	xPress() {
		this.rightRotation();
	}

	zRelease() {}
	xRelease() {}

	trigger() {
		if (!this.zPrev && this.zDown) this.zPress();
		if (this.zPrev && !this.zDown) this.zRelease();
		if (!this.xPrev && this.xDown) this.xPress();
		if (this.xPrev && !this.xDown) this.xRelease();

		this.zPrev = this.zDown;
		this.xPrev = this.xDown;
		this.NodeYPrev = this.node.y;
	}

	update(dt) {
		if (cc.find('Canvas').getComponent('GameMgr').startSignal) {
			if (!this.local) {
				let src = this.engineer.getComponent('engineer').DSend('Control');

				this.leftDown = src[0];
				this.rightDown = src[1];
				this.spaceDown = src[2];
				this.zDown = src[3];
				this.xDown = src[4];
			}

			if (this.local || cc.find('Canvas').getComponent('connect').isHost) {
				let camera = cc.find('Canvas/Main Camera');

				if (!this.control && camera.y - this.node.y >= 200) this.node.destroy();

				if (this.control) {
					if (this.NodeYPrev - this.node.y > 0.3 && this.node.y > camera.y) {
						this.unschedule(this.finishCur);
						this.scheduleOnce(this.finishCur, 1);
					}

					this.trigger();

					this.updateSpeed();
					this.Move(dt);
				}
			}
		}
	}

	updateSpeed() {
		this.getComponent(cc.RigidBody).gravityScale = 0.1;
		// let v = this.getComponent(cc.RigidBody).linearVelocity;
		// this.getComponent(cc.RigidBody).linearVelocity = cc.v2(v.x, this.spaceDown ? -500 : -50);

		if (this.leftDown != this.rightDown) this.Speed = this.leftDown ? -300 : 300;
		else this.Speed = 0;
	}

	Move(dt) {
		this.node.x += this.Speed * dt;

		if (this.spaceDown) {
			let v = this.getComponent(cc.RigidBody).linearVelocity;
			this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, Math.max(v.y - 250 * dt, -250));
		} else {
			let v = this.getComponent(cc.RigidBody).linearVelocity;
			this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, Math.max(v.y, -40));
		}
	}

	init(node: cc.Node, type: number, enable: boolean) {
		let camera = cc.find('Canvas/Main Camera');

		this.control = enable;
		this.create = enable;
		this.node.parent = node.getChildByName(String(type));
		this.node.position = cc.v2(485, 700 + camera.y);
		// cc.log(this.node.parent);
		//this.node.position = this.node.position.addSelf(node.position);
	}

	leftRotation() {
		this.node.angle += 90;
	}

	rightRotation() {
		this.node.angle -= 90;
	}

	moveRight() {
		this.node.position = cc.v2(this.node.x + 40, this.node.y);
	}

	moveLeft() {
		this.node.position = cc.v2(this.node.x - 40, this.node.y);
	}

	gravityEffect() {
		this.node.getComponent(cc.RigidBody).fixedRotation = false;
	}

	break() {
		if (!this.control && !this.nailed) {
			this.nailed = true;
			this.scheduleOnce(() => {
				this.getComponent(cc.RigidBody).gravityScale = 0.7;
				this.getComponent(cc.RigidBody).fixedRotation = false;
			}, 3);
		}
	}

	onBeginContact(contact, self, other) {
		//TODO: end of brick setting.
		let t = contact.getWorldManifold().normal;
		if (t.y < -0.99) {
			this.spaceDown = false;
			if (other.node.name == 'Ground') {
			} else if (other.tag == 10) {
			}
		}
	}

	nail() {
		if (cc.find('Canvas/UI/nail').getComponent('nailBtn').Using) {
			if (!this.control && !this.nailed) {
				this.nailed = true;
				cc.find('Canvas/UI/nail').getComponent('nailBtn').close();

				cc.find('Canvas/UI/nail').getComponent('nailBtn').nailCnt -= 1;
				cc.find('Canvas/UI/nail/nailcnt').getComponent(cc.Label).string =
					'x' + String(cc.find('Canvas/UI/nail').getComponent('nailBtn').nailCnt);

				let icon = cc.instantiate(cc.find('Canvas/UI/nail').getComponent('nailBtn').icon);
				icon.parent = this.node;

				switch (this.node.name) {
					case 'Block01':
						icon.position = cc.v2(35, -35);
						break;
					case 'Block03':
						icon.position = cc.v2(0, -35);
						break;
					case 'Block04':
						icon.position = cc.v2(0, -35);
						break;
					default:
						icon.position = cc.v2(0, 0);
				}

				this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 0);
				this.getComponent(cc.RigidBody).type = cc.RigidBodyType.Static;
			}
		}
	}
}
