import knight from './knight';

declare var firebase: any;

const { ccclass, property } = cc._decorator;

@ccclass
export default class GameMgr extends cc.Component {
	@property(cc.Node)
	Background: cc.Node = null;

	@property(cc.Node)
	Block: cc.Node = null;

	@property([cc.Prefab])
	BlockPrefabs: cc.Prefab[] = [];

	@property(cc.Node)
	Wall: cc.Node = null;

	@property([cc.Prefab])
	WallPrefabs: cc.Prefab[] = [];

	@property(cc.Node)
	Item: cc.Node = null;

	@property([cc.Prefab])
	ItemPrefabs: cc.Prefab[] = [];

	@property(cc.Node)
	sideProtect: cc.Node = null;

	@property(cc.Node)
	bottomProtect: cc.Node = null;

	@property(cc.Node)
	Camera: cc.Node = null;

	@property([cc.AudioClip])
	bgm: cc.AudioClip[] = [];

	@property([cc.AudioClip])
	clip: cc.AudioClip[] = [];

	private backgroundHeight = 384;
	private init = true;

	public startSignal = false;
	public isHost;

	public HP;
	public updateHP = true;

	public Water;
	public updateWater = true; //* add the water effect
	public enterDesert = false;

	public curScene = 0;
	public sceneScore = [20, 25, 25, 30];

	public timeStop = 1;
	public cameraSpeed = 0.2;

	public pausing = false;
	public GameFinished = false;

	DSend(target: string) {
		//* TODO;
		if (target == 'Wall') {
			let root = cc.find('Wall', this.node);

			let ret = [];
			for (let i = 0; i < 3; i++) {
				let allChildren = root.getChildByName(String(i)).children;

				let temp = [];

				for (let i = 0; i < allChildren.length; i++)
					temp.push({
						position: allChildren[i].position,
						angle: allChildren[i].angle,
					});

				ret.push(temp);
			}

			return ret;
		} else if (target == 'DefaultBrick') {
			let root = cc.find('DefaultBrick_Container', this.node);

			let ret = [];
			for (let i = 0; i < 10; i++) {
				let allChildren = root.getChildByName(String(i)).children;

				let temp = [];

				for (let i = 0; i < allChildren.length; i++)
					temp.push({
						position: allChildren[i].position,
						angle: allChildren[i].angle,
					});

				ret.push(temp);
			}

			return ret;
		} else if (target == 'Item') {
			let root = cc.find('Item_Container', this.node);

			let ret = [];
			for (let i = 0; i < 15; i++) {
				let allChildren = root.getChildByName(String(i)).children;

				let temp = [];

				for (let i = 0; i < allChildren.length; i++)
					temp.push({
						position: allChildren[i].position,
						angle: allChildren[i].angle,
						active: allChildren[i].active,
					});

				ret.push(temp);
			}

			return ret;
		} else if (target == 'Data') {
			let nail = cc.find('Canvas/UI/nail').getComponent('nailBtn').nailCnt;
			let clock = cc.find('Canvas/UI/clock').getComponent('clockBtn').clockCnt;
			let ice = cc.find('Canvas/UI/IceBar').getComponent(cc.ProgressBar).progress;

			return [this.Camera.y, this.sideProtect.y, this.bottomProtect.y, this.Background.y, this.HP, this.Water, nail, clock, ice];
		}
	}

	DGet(src, target: string) {
		if (target == 'Wall') {
			let root = cc.find('Wall', this.node);

			for (let i = 0; i < 3; i++) {
				let Count = root.getChildByName(String(i)).childrenCount;
				let allChildren = root.getChildByName(String(i)).children;

				if (src && src[i] != undefined) {
					for (let k = 0; k < src[i].length; k++) {
						if (k < Count) {
							allChildren[k].position = src[i][k].position;
							allChildren[k].angle = src[i][k].angle;
						} else {
							let block = cc.instantiate(this.WallPrefabs[i]);
							block.parent = this.Wall.getChildByName(String(i));

							block.position = src[i][k].position;
							block.angle = src[i][k].angle;
						}
					}

					for (let k = src[i].length; k < Count; k++) root.getChildByName(String(i)).removeChild(allChildren[k]);
				}
			}
		} else if (target == 'DefaultBrick') {
			let root = cc.find('DefaultBrick_Container', this.node);

			for (let i = 0; i < 10; i++) {
				if (!src || !src[i]) continue;

				let Count = root.getChildByName(String(i)).childrenCount;
				let allChildren = root.getChildByName(String(i)).children;
				if (src && src[i] != undefined) {
					for (let k = 0; k < src[i].length; k++) {
						if (k < Count) {
							allChildren[k].position = src[i][k].position;
							allChildren[k].angle = src[i][k].angle;
						} else {
							let block = cc.instantiate(this.BlockPrefabs[i]);
							block.parent = this.Block.getChildByName(String(i));

							block.position = src[i][k].position;
							block.angle = src[i][k].angle;
						}
					}

					for (let k = src[i].length; k < Count; k++) root.getChildByName(String(i)).removeChild(allChildren[k]);
				}
			}
		} else if (target == 'Item') {
			let root = cc.find('Item_Container', this.node);

			for (let i = 0; i < 15; i++) {
				if (!src || !src[i]) continue;

				let Count = root.getChildByName(String(i)).childrenCount;
				let allChildren = root.getChildByName(String(i)).children;
				if (src && src[i] != undefined) {
					for (let k = 0; k < src[i].length; k++) {
						if (k < Count) {
							allChildren[k].position = src[i][k].position;
							allChildren[k].angle = src[i][k].angle;
							allChildren[k].active = src[i][k].active;
						} else {
							let block = cc.instantiate(this.ItemPrefabs[i]);
							block.parent = this.Item.getChildByName(String(i));

							block.position = src[i][k].position;
							block.angle = src[i][k].angle;
							block.active = src[i][k].active;
						}
					}

					for (let k = src[i].length; k < Count; k++) root.getChildByName(String(i)).removeChild(allChildren[k]);
				}
			}
		} else if (target == 'Data') {
			if (src) {
				this.Camera.y = src[0];
				this.sideProtect.y = src[1];
				this.bottomProtect.y = src[2];
				this.Background.y = src[3];
				this.HP = src[4];
				this.Water = src[5];
				cc.find('Canvas/UI/nail').getComponent('nailBtn').nailCnt = src[6];
				cc.find('Canvas/UI/nail/nailcnt').getComponent(cc.Label).string = 'x' + String(src[6]);
				cc.find('Canvas/UI/clock').getComponent('clockBtn').clockCnt = src[7];
				cc.find('Canvas/UI/clock/clockcnt').getComponent(cc.Label).string = 'x' + String(src[7]);
				cc.find('Canvas/UI/IceBar').getComponent(cc.ProgressBar).progress = src[8];
			}
		}
	}


	onKeyUp(evt) {
		let key = cc.macro.KEY;
		let root = cc.find('Canvas').getComponent('engineer');

		//* check if host.
		if (this.startSignal) {
			switch (evt.keyCode) {
				case key.r:
					let ra = Math.floor(Math.random() * 10);
					let ra1 = Math.floor(Math.random() * 10);
					let ra2 = Math.floor(Math.random() * 10);

					root.setArr(ra, ra1, ra2);
					root.createBrick();
					break;
			}
		}
	}

	onLoad() {
		cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);

		cc.audioEngine.stopAll();
		for (let i = 1; i < 4; i++) cc.find('Canvas/background/background' + i).runAction(cc.fadeOut(0));
		this.scheduleOnce(() => {
			cc.audioEngine.playMusic(this.bgm[0], true);
			this.schedule(() => {
				var num = 1;
				var callback = function () {
					cc.audioEngine.setMusicVolume(num);
					num -= 0.1;
					if (num <= 0) {
						this.unschedule(callback);
						// cc.log('changesong');
						cc.audioEngine.playMusic(this.bgm[this.curScene], true);
						cc.audioEngine.setMusicVolume(1);
					}
				};
				if (this.HP != 0) this.schedule(callback, 0.6);
				else cc.audioEngine.stopMusic();

				this.scheduleOnce(() => {
					cc.find('Canvas/background/background' + ((this.curScene + 3) % 4)).runAction(cc.fadeOut(3));
				}, 3);

				this.cameraSpeed += 0.07;
				this.curScene = (this.curScene + 1) % 4;
				// cc.log('changescene');
				this.scheduleOnce(() => {
					cc.find('Canvas/background/background' + this.curScene).runAction(cc.fadeIn(1));
				}, 3);

				if (this.curScene == 3) {
					for (let i = 0; i < 3; i++) {
						let ra = Math.floor(Math.random() * 10);

						if (cc.find('Canvas').getComponent('engineer').order_li[i] == 10) cc.find('Canvas').getComponent('engineer').order_li[i] = ra;
					}

					let arr = cc.find('Canvas').getComponent('engineer').order_li;
					cc.find('Canvas').getComponent('engineer').setArr(arr[0], arr[1], arr[2]);

					if (!cc.find('Canvas').getComponent('engineer').controlling) cc.find('Canvas').getComponent('engineer').createBrick();
				}
			}, 90);
		}, 6);

		this.schedule(() => {
			if (this.startSignal) {
				this.FHP(1.5);
			}
		}, 1);

		cc.director.getPhysicsManager().enabled = true;
		this.HP = 200;
		this.Water = 100;

		this.schedule(() => {
			if (this.curScene != 1) {
				this.FWater(8);
			}
		}, 1);
	}

	start() {}

	FHP(num: number) {
		if (this.isHost) {
			if (this.updateHP) {
				this.HP += num;

				if (this.HP <= 0) {
					this.HP = 0;
					cc.find('Canvas/knight').getComponent('knight').isDead = true;
				}

				if (this.HP > 200) this.HP = 200;

				return this.HP > 0;
			}
		}
		return undefined;
	}

	FWater(num: number) {
		if (this.isHost) {
			if (this.updateWater) {
				this.Water += num;

				if (this.Water < 0) this.Water = 0;
				if (this.Water > 100) this.Water = 100;

				return this.Water > 0;
			}
		}
		return undefined;
	}

	update(dt) {
		if (this.isHost != undefined && this.startSignal) {
			cc.find('UI/Life/lifeBar', this.node).scaleX = this.HP / 100;
			cc.find('UI/Water/waterBar', this.node).scaleX = this.Water / 100;

			if (this.HP == 0 && this.updateHP) {
				this.updateHP = false;
				this.updateWater = false;

				this.gameOver();
			}

			if (this.isHost) {
				if (this.init) {
					let cnt = 0;
					let cnt_2 = 0;
					let posY = 760;
					let posY_s = 760;
					let blockSize = [43.8, 80.4, 156];

					//* static block
					this.schedule(() => {
						if (Math.floor(720 + cnt * 40) - this.Camera.y < 700) {
							let BlockParameter = [
								[3, 1, 0, 0, 0, 0, 0, 0, 0, 0],
								[0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
								[0, 0, 0, 3, 3, 4, 7, 5, 0, 0],
								[0, 0, 0, 0, 0, 0, 0, 0, 2, 3],
							];

							let i = this.chooseBlock(BlockParameter[this.curScene]);
							let block = cc.instantiate(this.BlockPrefabs[i]);
							let posX = 100 + Math.floor(Math.random() * 660);
							block.parent = this.Block.getChildByName(String(i));

							if (posX < 92) posX = 92;
							if (posX + block.height > 820) posX = 820 - block.height;
							block.position = cc.v2(posX, Math.floor(720 + cnt * 40));
							cnt += Math.random() * 3 + 2;
						}
					}, 0.05);

					// TODO firebase.
					//* generate item
					this.schedule(() => {
						if (Math.floor(720 + cnt_2 * 25) - this.Camera.y < 700) {
							//* nail, clock, water * 2, ice, bubble
							let ItemParameter = [
								[2, 1, 0, 0, 0, 0],
								[1, 0, 7, 2, 0, 0],
								[0, 1, 0, 0, 0, 7],
								[1, 1, 0, 0, 5, 0],
							];

							let j = this.chooseBlock(ItemParameter[this.curScene]);

							if (j == 5) {
								//* bubble
								let ra = Math.floor(Math.random() * 10);
								j += ra;
								// 5 ~ 14
							}

							let item = cc.instantiate(this.ItemPrefabs[j]);
							let posX = 100 + Math.floor(Math.random() * 660);
							item.parent = this.Item.getChildByName(String(j));

							if (posX < 92) posX = 92;
							if (posX + item.width > 820) posX = 820 - item.width;
							item.position = cc.v2(posX, Math.floor(720 + cnt_2 * 25));
							cnt_2 += Math.random() * 3 + 3;
						}
					}, 0.05);

					//left wall
					this.schedule(() => {
						if (posY - this.Camera.y < 700) {
							let rand = Math.floor(Math.random() * 8) % 7;
							let j = this.chooseBlock([5, 3, 1]);
							if (rand == 1) {
								posY += blockSize[j] + 14;
							} else {
								let block = cc.instantiate(this.WallPrefabs[j]);
								let posX = 68;
								block.parent = this.Wall.getChildByName(String(j));
								posY += block.width / 2;
								block.position = cc.v2(posX, posY);
								posY += block.width / 2 + 14;
							}
						}
					}, 0.05);

					//right wall
					this.schedule(() => {
						if (posY_s - this.Camera.y < 700) {
							let rand = Math.floor(Math.random() * 8) % 7;
							let k = this.chooseBlock([5, 3, 1]);
							if (rand == 1) {
								posY_s += blockSize[k] + 14;
							} else {
								let block = cc.instantiate(this.WallPrefabs[k]);
								let posX = 845;
								block.parent = this.Wall.getChildByName(String(k));
								posY_s += block.width / 2;
								block.position = cc.v2(posX, posY_s);
								posY_s += block.width / 2 + 14;
							}
						}
					}, 0.05);

					this.schedule(() => {
						cc.find('Canvas/UI/Score/number').getComponent('score').add(this.sceneScore[this.curScene]);
					}, 1);

					this.init = false;
				}

				if (this.curScene == 1 && !this.enterDesert) {
					this.enterDesert = true;
					this.schedule(this.reduceWater, 1);
				}
				if (this.curScene != 1 && this.enterDesert) {
					this.enterDesert = false;
					this.unschedule(this.reduceWater);
				}

				if (cc.find('Canvas/knight').getComponent('knight').ejecting) {
					let diff = cc.find('Canvas/knight').getComponent('knight').node.y - this.Camera.y;

					this.Camera.y += diff;
					this.sideProtect.y += diff;
					this.bottomProtect.y += diff;
					this.Background.y += diff;
				} else {
					this.Camera.y += this.cameraSpeed * this.timeStop;
					this.sideProtect.y += this.cameraSpeed * this.timeStop;
					this.bottomProtect.y += this.cameraSpeed * this.timeStop;

					let player = cc.find('Canvas/knight');
					if (player.getComponent('knight').inCamera) {
						player.y += this.cameraSpeed * this.timeStop;
					}
				}

				if (this.Camera.y - this.Background.y >= this.backgroundHeight) this.Background.y += this.backgroundHeight;
			}
		} else this.isHost = this.getComponent('connect').isHost;
	}

	reduceWater() {
		if (this.timeStop && this.isHost) {
			this.FWater(-2.5);

			if (this.Water <= 30) {
				this.FHP(-5);
			}
		}
	}

	pauseFunc() {
		if (this.isHost) {
			if (cc.find('Canvas/UI/clock').getComponent('clockBtn').clockCnt != 0) {
				if (!this.pausing) {
					this.pausing = true;
					this.timeStop = 0;

					cc.find('Canvas/UI/clock').getComponent('clockBtn').clockCnt -= 1;
					cc.find('Canvas/UI/clock/clockcnt').getComponent(cc.Label).string =
						'x' + String(cc.find('Canvas/UI/clock').getComponent('clockBtn').clockCnt);

					this.scheduleOnce(() => {
						this.timeStop = 1;
						this.pausing = false;
					}, 5);
				}
			}
		}
	}

	gameOver() {
		this.GameFinished = true;
		let database = firebase.database();
		let room = this.getComponent('connect').room;

		if (room != '') database.ref('room/' + room + '/status').set('full');

		let target = cc.find('UI/Gameover', this.node);

		target.active = true;

		target.runAction(cc.sequence(cc.fadeOut(0), cc.moveTo(0, -190, 0), cc.delayTime(2), cc.fadeIn(2)));
		cc.audioEngine.stopMusic();
		this.scheduleOnce(() => {
			cc.audioEngine.playEffect(this.clip[0], false);
		}, 1);
	}

	chooseBlock(prob) {
		let rand = Math.random();
		//let prob = [3, 1];
		let sum = prob.reduce((a, b) => a + b);
		for (let i = 1; i < prob.length; i++) prob[i] += prob[i - 1];
		for (let i = 0; i < prob.length; i++) {
			prob[i] /= sum;
			if (rand <= prob[i]) return i;
		}
	}
}
