const { ccclass, property } = cc._decorator;

@ccclass
export default class staticBlock extends cc.Component {
	private Camera = null;
	public nailed = false;

	onLoad() {
		this.node.on(cc.Node.EventType.MOUSE_DOWN, this.nail, this);
		this.Camera = cc.find('Canvas/Main Camera');
	}
	update(dt) {
		if (cc.find('Canvas').getComponent('GameMgr').startSignal) {
			if (this.Camera.y - this.node.y >= 200) {
				// cc.log('destroy');
				this.node.destroy();
			}
		}
	}

	break() {
		if (!this.nailed) {
			this.nailed = true;
			this.scheduleOnce(() => {
				this.getComponent(cc.RigidBody).gravityScale = 0.7;
				this.getComponent(cc.RigidBody).type = cc.RigidBodyType.Dynamic;
				this.getComponent(cc.RigidBody).fixedRotation = false;
			}, 3);
		}
	}

	nail() {
		if (cc.find('Canvas/UI/nail').getComponent('nailBtn').Using) {
			if (!this.nailed) {
				this.nailed = true;
				cc.find('Canvas/UI/nail').getComponent('nailBtn').close();

				cc.find('Canvas/UI/nail').getComponent('nailBtn').nailCnt -= 1;
				cc.find('Canvas/UI/nail/nailcnt').getComponent(cc.Label).string =
					'x' + String(cc.find('Canvas/UI/nail').getComponent('nailBtn').nailCnt);

				let icon = cc.instantiate(cc.find('Canvas/UI/nail').getComponent('nailBtn').icon);
				icon.parent = this.node;

				switch (this.node.name) {
					case 'staticBlock':
						icon.position = cc.v2(35, 35);
						break;
					case 'staticBlock2':
						icon.position = cc.v2(70, 35);
						break;
					case 'fireBlock1':
						icon.position = cc.v2(35, 35);
						break;
					case 'fireBlock1_2':
						icon.position = cc.v2(35, 35);
						break;
					case 'fireBlock2':
						icon.position = cc.v2(70, 35);
						break;
					case 'healBlock1':
						icon.position = cc.v2(35, 35);
						break;
					case 'healBlock2':
						icon.position = cc.v2(70, 35);
						break;
					case 'iceBlock1':
						icon.position = cc.v2(35, 35);
						break;
					case 'iceBlock2':
						icon.position = cc.v2(70, 35);
						break;
					default:
						icon.position = cc.v2(0, 0);
				}

				this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 0);
				this.getComponent(cc.RigidBody).type = cc.RigidBodyType.Static;
			}
		}
	}

	onBeginContact(contact, self, other) {
		// cc.log(self.node.name, other.node.name);
	}
}
