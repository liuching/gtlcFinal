// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property([cc.Prefab])
	Bubbles: cc.Prefab[] = [];

    private Camera = null;

	onLoad() {
		this.Camera = cc.find('Canvas/Main Camera');
	}
	update(dt) {
		if (this.Camera.y - this.node.y >= 200) {
			// cc.log('destroy');
			this.node.destroy();
		}
	}

    createBubble() {
        
    }


    onBeginContact(contact, self, other) {
        let t = contact.getWorldManifold().normal;
        if(t.y == -1 && (other.name == 'vacBlock1' || other.name == 'vacBlock2')){
            // TODO 扣血
        }
        else{

        }
    }
    // update (dt) {}
}
