// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class bubble extends cc.Component {
	private Camera = null;
	@property(cc.AudioClip)
	getEffect: cc.AudioClip = null;
	onLoad() {
		this.Camera = cc.find('Canvas/Main Camera');
	}

	start() {}

	setPosition() {}

	find_first_10() {
		let temp = cc.find('Canvas').getComponent('engineer').order_li;

		let ans = -1;
		for (let i = 0; i < 3; i++)
			if (temp[i] == 10) {
				ans = i;
				break;
			}

		return ans;
	}

	update(dt) {
		if (this.Camera.y - this.node.y >= 200) this.node.destroy();
	}

	onBeginContact(contact, self, other) {
		if (other.node.name == 'knight') {
			cc.audioEngine.playEffect(this.getEffect, false);
			let temp = cc.find('Canvas').getComponent('engineer').order_li;

			let block_num = Number(self.node.name.slice(-1));
			// cc.log(block_num);
			block_num = block_num == 0 ? 9 : block_num - 1;

			let check = this.find_first_10();

			if (check == 0) {
				if (!cc.find('Canvas').getComponent('engineer').controlling) {
					// push into arr and call create.
					cc.find('Canvas').getComponent('engineer').controlling = true;
					cc.find('Canvas').getComponent('engineer').setArr(block_num, 10, 10);
					cc.find('Canvas').getComponent('engineer').createBrick();
				} else {
					//push into arr
					cc.find('Canvas').getComponent('engineer').setArr(block_num, 10, 10);
				}
			} else if (check == 1) {
				cc.find('Canvas').getComponent('engineer').setArr(temp[0], block_num, 10);
			} else if (check == 2) {
				cc.find('Canvas').getComponent('engineer').setArr(temp[0], temp[1], block_num);
			}

			this.node.active = false;
		}
	}
}
