// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
declare var firebase: any;
const { ccclass, property } = cc._decorator;

@ccclass
export default class connect extends cc.Component {
	// LIFE-CYCLE CALLBACKS:

	@property(cc.Node)
	Knight: cc.Node = null;

	public isHost;

	public user;
	public room = '';

	public RJumper;
	public REngineer;
	public RWall;
	public RData;
	public RItem;
	public RDefaultBrick;
	public RBrick;
	public RBrickSign;
	public RScore;

	public local = false;

	updateFirebase() {
		if (!this.local) {
			if (cc.find('Canvas').getComponent('GameMgr').startSignal) {
				if (this.isHost != undefined) {
					if (this.isHost) {
						//* update all data to database.
						this.RJumper.set(this.Knight.getComponent('knight').DSend());
						this.RWall.set(this.getComponent('GameMgr').DSend('Wall'));
						this.RData.set(this.getComponent('GameMgr').DSend('Data'));
						this.RItem.set(this.getComponent('GameMgr').DSend('Item'));
						this.RDefaultBrick.set(this.getComponent('GameMgr').DSend('DefaultBrick'));
						this.RBrick.set(this.getComponent('engineer').DSend('Brick'));
						this.RBrickSign.set(this.getComponent('engineer').DSend('BlockSign'));
						this.RScore.set(cc.find('UI/Score/number', this.node).getComponent('score').DSend());

						this.REngineer.once('value', data => {
							this.getComponent('engineer').DGet(data.val(), 'Control');
						});
					} else {
						//* get data from database and update game.
						this.REngineer.set(this.getComponent('engineer').DSend('Control'));

						this.RJumper.once('value', data => {
							this.Knight.getComponent('knight').DGet(data.val());
						});

						this.RWall.once('value', data => {
							this.getComponent('GameMgr').DGet(data.val(), 'Wall');
						});

						this.RData.once('value', data => {
							this.getComponent('GameMgr').DGet(data.val(), 'Data');
						});

						this.RItem.once('value', data => {
							this.getComponent('GameMgr').DGet(data.val(), 'Item');
						});

						this.RDefaultBrick.once('value', data => {
							this.getComponent('GameMgr').DGet(data.val(), 'DefaultBrick');
						});

						this.RBrick.once('value', data => {
							this.getComponent('engineer').DGet(data.val(), 'Brick');
						});

						this.RBrickSign.once('value', data => {
							this.getComponent('engineer').DGet(data.val(), 'BlockSign');
						});

						this.RScore.once('value', data => {
							cc.find('UI/Score/number', this.node).getComponent('score').DGet(data.val());
						});
					}
				}
			}
		}
	}

	onLoad() {
		let database = firebase.database();

		firebase.auth().onAuthStateChanged(() => {
			this.user = firebase.auth().currentUser;

			if (this.user) {
				database.ref('player/' + this.user.uid).once('value', item => {
					if (item.val()) {
						if (item.val() == 'local') {
							this.isHost = true;
							this.local = true;
						} else {
							this.room = item.val();

							//* check if host.
							database.ref('room/' + this.room + '/player/0/name').once('value', item => {
								// cc.log('host validate.');
								// cc.log(item.val() + ', ' + this.user.uid);

								this.isHost = item.val() == this.user.uid;

								// cc.log(isHost ? 'accepted.' : 'denied.');
							});

							//* set database ref.
							this.RJumper = database.ref('room/' + this.room + '/player/0/control');
							this.REngineer = database.ref('room/' + this.room + '/player/1/control');
							this.RWall = database.ref('room/' + this.room + '/wall');
							this.RData = database.ref('room/' + this.room + '/data');
							this.RItem = database.ref('room/' + this.room + '/item');
							this.RDefaultBrick = database.ref('room/' + this.room + '/defaultBrick');
							this.RBrick = database.ref('room/' + this.room + '/brick');
							this.RBrickSign = database.ref('room/' + this.room + '/brickSign');
							this.RScore = database.ref('room/' + this.room + '/score');

							this.schedule(this.updateFirebase, 0.001);
						}
					} else cc.director.loadScene('RoomList');
				});
			} else cc.director.loadScene('Login');
		});
	}

	start() {}

	back() {
		this.unschedule(this.updateFirebase);

		let database = firebase.database();

		database.ref('player/' + this.user.uid).once('value', item => {
			if (item.val() == 'local') {
				database.ref('player/' + this.user.uid).remove();
				cc.director.loadScene('RoomList');
			} else cc.director.loadScene('inRoom');
		});
	}

	// update (dt) {}
}
