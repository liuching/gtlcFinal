// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class cola extends cc.Component {
	public shake_cnt = 0;
	public ready = false;

	start() {
		if (this.ready)
			this.node.on(
				cc.Node.EventType.MOUSE_DOWN,
				function () {
					this.shakecola();
				},
				this
			);
	}

	onLoad() {
		this.node.active = false;
	}

	//當冰塊集滿的時候呼叫此function 顯示cola 在10秒後會再次隱藏
	trigger() {
		this.node.active = true;
		this.ready = true;
		this.schedule(function () {
			cc.find('Canvas/UI/IceBar').getComponent(cc.ProgressBar).progress -= 0.001;
			// cc.log(cc.find('Canvas/UI/IceBar').getComponent(cc.ProgressBar).progress);

			if (this.ready && cc.find('Canvas/UI/IceBar').getComponent(cc.ProgressBar).progress <= 0.001) {
				this.ready = false;
				this.node.active = false;
				cc.find('Canvas/knight').getComponent('knight').ejection(this.shake_cnt);
				this.shake_cnt = 0;
			}
		}, 0.01);
	}

	//計算搖動cola次數
	shakecola() {
		let Act = cc.sequence(cc.moveBy(0.05, 0, 35), cc.moveBy(0.05, 0, -35));
		this.node.runAction(Act);
		if (this.shake_cnt <= 100) this.shake_cnt += 1;
		// cc.log('click cola');
	}

	// update (dt) {}
}
