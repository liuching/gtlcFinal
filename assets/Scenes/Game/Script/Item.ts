const { ccclass, property } = cc._decorator;

@ccclass
export default class Item extends cc.Component {
	private Camera = null;
	private ContactOnce = false;

	public clockCnt = 0;

	@property(cc.AudioClip)
	getclip: cc.AudioClip = null;

	onLoad() {
		this.Camera = cc.find('Canvas/Main Camera');
	}
	update(dt) {
		if (cc.find('Canvas').getComponent('GameMgr').startSignal) {
			if (this.Camera.y - this.node.y >= 200) {
				this.node.destroy();
			}
		}
	}

	onBeginContact(contact, self, other) {
		if (other.node.name == 'knight') {
			if (!this.ContactOnce) {
				this.ContactOnce = true;
				this.node.active = false;
				if (self.node.name == 'Nail') {
					let audioID = cc.audioEngine.playEffect(this.getclip, false);
					cc.audioEngine.setVolume(audioID, 0.3);

					cc.find('Canvas/UI/nail').getComponent('nailBtn').nailCnt += 1;
					cc.find('Canvas/UI/nail/nailcnt').getComponent(cc.Label).string =
						'x' + String(cc.find('Canvas/UI/nail').getComponent('nailBtn').nailCnt);
				}
				if (self.node.name == 'Clock') {
					let audioID = cc.audioEngine.playEffect(this.getclip, false);
					cc.audioEngine.setVolume(audioID, 0.3);

					cc.find('Canvas/UI/clock').getComponent('clockBtn').clockCnt += 1;
					cc.find('Canvas/UI/clock/clockcnt').getComponent(cc.Label).string =
						'x' + String(cc.find('Canvas/UI/clock').getComponent('clockBtn').clockCnt);
				}
				//*drink water
				if (self.node.name == 'Water_bottle_sm') {
					cc.audioEngine.playEffect(this.getclip, false);
					cc.find('Canvas').getComponent('GameMgr').FWater(20);
					this.node.destroy();
				}
				if (self.node.name == 'Water_bottle_lg') {
					cc.audioEngine.playEffect(this.getclip, false);
					cc.find('Canvas').getComponent('GameMgr').FWater(50);
					this.node.destroy();
				}

				if (self.node.name == 'Ice') {
					cc.audioEngine.playEffect(this.getclip, false);
					this.node.destroy();
				}
			}
		}
	}
}
