const { ccclass, property } = cc._decorator;

@ccclass
export default class desert extends cc.Component {
	@property({ type: cc.AudioClip })
	WalkWalk_sound: cc.AudioClip = null;
	@property({ type: cc.AudioClip })
	BreakSound: cc.AudioClip = null;
	@property({ type: cc.AudioClip })
	BottleSound: cc.AudioClip = null;

	private Camera = null;

	private anim = null; //this will use to get animation component
	private animateState = null; //this will use to record animationState

	// LIFE-CYCLE CALLBACKS:

	onLoad() {
		this.Camera = cc.find('Canvas/Main Camera');
		this.anim = this.getComponent(cc.Animation);
	}

	update(dt) {
		if (cc.find('Canvas').getComponent('GameMgr').startSignal) {
			if (this.Camera.y - this.node.y >= 200) {
				this.node.destroy();
			}
		}
	}

	onBeginContact(contact, self, other) {
		if (other.node.group == 'player') {
			if (contact.getWorldManifold().normal.y > 0.99) {
				let action = cc.sequence(cc.moveBy(0.1, 2, 0), cc.moveBy(0.1, -2, 0));
				//cc.moveBy(0.1,0.8,0),cc.moveBy(0.1,-0.8,0),cc.scaleTo(0.1,0.8,1.2),cc.scaleTo(0.2,1,1)
				cc.audioEngine.playEffect(this.BreakSound, false);

				this.scheduleOnce(function () {
					this.node.destroy();
					this.unscheduleAllCallbacks();
				}, 5);
				this.scheduleOnce(function () {
					//cc.audioEngine.playEffect(this.BottleSound,false);
					this.anim.play('sand_break');
				}, 4.7);
				this.schedule(function () {
					this.node.runAction(action);
				}, 0.2);
			}
		}
	}
}
