// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class ice extends cc.Component {
	// onLoad () {}

	//public trigger = false;

	start() {}

	// update (dt) {}

	onBeginContact(contact, self, other) {
		if (other.node.name == 'knight') {
			if (cc.find('Canvas').getComponent('GameMgr').isHost) {
				if (cc.find('Canvas/UI/IceBar').getComponent(cc.ProgressBar).progress < 1 && !cc.find('Canvas/UI/cola').active) {
					cc.find('Canvas/UI/IceBar').getComponent(cc.ProgressBar).progress += 0.2;
					if (cc.find('Canvas/UI/IceBar').getComponent(cc.ProgressBar).progress >= 0.99) {
						cc.find('Canvas/UI/cola').getComponent('cola').trigger();
					}
				}
			}
		}
	}
}
