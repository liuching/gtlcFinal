const { ccclass, property } = cc._decorator;

@ccclass
export default class knight extends cc.Component {
	public isHost;

	public protect = false;
	public jumpLock = true;

	@property(cc.Animation)
	animator: cc.Animation = null;
	@property(cc.AudioClip)
	jumpEffect: cc.AudioClip = null;
	@property(cc.AudioClip)
	fallEffect: cc.AudioClip = null;
	@property(cc.AudioClip)
	pongEffect: cc.AudioClip = null;
	@property(cc.AudioClip)
	click: cc.AudioClip = null;
	@property(cc.AudioClip)
	reviveclip: cc.AudioClip = null;

	public animationState;
	public remoteAniState;

	//* Control.
	public leftDown: boolean = false;
	public rightDown: boolean = false;
	public spaceDown: boolean = false;
	public downDown: boolean = false;

	//* check positive trigger.
	public spacePrev: boolean = false;
	public downPrev: boolean = false;

	//* Player property.
	public Speed: number = 0;
	public jumping: boolean = false; //* is jumping.
	public falling: boolean = false;
	public gathering: boolean = false;
	public jumpDir: number = 0;
	public canJump: boolean = false; //* can I jump if space is released?
	public onGround: boolean = true;
	public breakBlock: boolean = false;
	public reviving: boolean = false;
	public onIce: boolean = false;
	public onFire: boolean = false;
	public onHeal: boolean = false;
	public isDead: boolean = false;
	public inCamera: boolean = false;

	public ejecting: boolean = false;
	public ejectingParam: number = 0;

	//* Jump parameter.
	public initialX: number = 100;
	public currentEnergy: number = 1;
	public JumpScale: number = 20;
	public maxEnergy: number = 580;
	public totalTime: number = 0.4;

	public preventJumpFail = () => {
		this.onGround = true;
		this.jumping = false;
		this.falling = false;
	};

	public gatherEnergy = () => {
		if (this.currentEnergy >= this.maxEnergy) {
			this.unschedule(this.gatherEnergy);

			this.gathering = false;
			if (this.canJump) {
				this.canJump = false;
				this.jump();
			}
		} else this.currentEnergy += (this.maxEnergy - this.initialX) / this.JumpScale;
	};

	//* firebase.

	DSend() {
		let ret = {
			position: this.node.position,
			velocity: this.getComponent(cc.RigidBody).linearVelocity,
			scaleX: this.node.scaleX,
			leftDown: this.leftDown,
			rightDown: this.rightDown,
			spaceDown: this.spaceDown,
			downDown: this.downDown,
			animation: this.animationState.name,
			fallingProtect: this.protect,
			jumpLock: this.jumpLock,
		};

		return ret;
	}

	DGet(src) {
		this.node.position = src.position;
		this.getComponent(cc.RigidBody).linearVelocity = src.velocity;
		this.node.scaleX = src.scaleX;
		this.leftDown = src.leftDown;
		this.rightDown = src.rightDown;
		this.spaceDown = src.spaceDown;
		this.downDown = src.downDown;
		this.remoteAniState = src.animation;

		if (src.fallingProtect != this.protect) {
			this.protect = !this.protect;
			cc.audioEngine.playEffect(this.click, false);
			cc.find('Canvas/UI/FallingProtect').getComponent(cc.Label).string = 'FallingProtect: ' + (this.protect ? 'ON' : 'OFF');
			cc.find('Canvas/UI/FallingProtect').color = this.protect ? cc.color(170, 30, 30) : cc.color(0, 0, 0);
		}

		if (src.jumpLock != this.jumpLock) {
			this.jumpLock = !this.jumpLock;
			cc.audioEngine.playEffect(this.click, false);
			cc.find('Canvas/UI/JumpLock').getComponent(cc.Label).string = 'Jump Lock: ' + (this.jumpLock ? 'ON' : 'OFF');
			cc.find('Canvas/UI/JumpLock').color = this.jumpLock ? cc.color(170, 30, 30) : cc.color(0, 0, 0);
		}
	}

	jump() {
		//* check if host.
		if (this.isHost) {
			cc.audioEngine.playEffect(this.jumpEffect, false);
			this.node.y += cc.find('Canvas').getComponent('GameMgr').cameraSpeed * 3;
			if (this.jumpLock) {
				if (this.leftDown != this.rightDown) this.jumpDir = this.leftDown ? -1 : 1;
				else this.jumpDir = 0;
			} else this.jumpDir = 0;

			this.scheduleOnce(this.preventJumpFail, 2);

			this.jumping = true;
			this.onGround = false;
			this.onIce = false;
			this.onFire = false;
			this.onHeal = false;

			this.getComponent(cc.RigidBody).linearVelocity = cc.v2(
				((this.currentEnergy / this.maxEnergy) * 150 + 250) * this.jumpDir,
				this.currentEnergy
			);
		}
	}

	spacePress() {
		if (this.getComponent(cc.RigidBody).linearVelocity.y < 0.01 && !this.jumping) {
			this.schedule(this.gatherEnergy, this.totalTime / this.JumpScale);
			this.currentEnergy = this.initialX;

			this.gathering = true;
			this.canJump = true;
		}
	}

	spaceRelease() {
		if (this.canJump) {
			this.gathering = false;
			this.canJump = false;
			this.unschedule(this.gatherEnergy);
			this.jump();
		}
	}

	downPress() {
		if (this.jumping) {
			this.falling = true;

			if (this.getComponent(cc.RigidBody).linearVelocity.y != 0) this.drop();
		}
	}

	downRelease() {}

	updateTrigger() {
		if (!this.spacePrev && this.spaceDown) this.spacePress();
		if (this.spacePrev && !this.spaceDown) this.spaceRelease();

		if (!this.downPrev && this.downDown) this.downPress();
		if (this.downPrev && !this.downDown) this.downRelease();

		this.spacePrev = this.spaceDown;
		this.downPrev = this.downDown;
	}

	onLoad() {
		this.schedule(() => {
			if (cc.find('Canvas').getComponent('GameMgr').startSignal && this.onFire)
				if (!cc.find('Canvas').getComponent('GameMgr').FHP(-5)) this.isDead = true;
		}, 0.5);

		this.schedule(() => {
			if (cc.find('Canvas').getComponent('GameMgr').startSignal && this.onHeal) cc.find('Canvas').getComponent('GameMgr').FHP(0.5);
		}, 0.05);

		cc.director.getPhysicsManager().enabled = true;
		cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
		cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);

		this.animator = this.getComponent(cc.Animation);
		this.animationState = this.animator.play();
	}

	start() {}

	revive() {
		this.reviving = true;
		this.jumping = false;
		this.falling = false;
		this.gathering = false;
		this.canJump = false;
		this.onGround = true;
		this.onIce = false;
		this.onFire = false;
		this.onHeal = false;

		this.getComponent(cc.RigidBody).active = false;

		let callRevive = cc.callFunc(() => {
			let camera = cc.find('Canvas/Main Camera');

			let ret = cc.callFunc(() => {
				this.getComponent(cc.RigidBody).active = true;

				this.inCamera = false;
				this.reviving = false;
				this.jumping = true;
				this.falling = true;

				this.drop();
			}, this);

			let moveUp = cc.callFunc(() => {
				this.inCamera = true;
			}, this);

			cc.audioEngine.playEffect(this.reviveclip, false);

			this.node.runAction(
				cc.sequence(cc.moveTo(0.1, cc.v2(-190, -400 + camera.y)), cc.spawn(moveUp, cc.moveBy(2, cc.v2(0, 600))), cc.delayTime(2), ret)
			);
		}, this);

		this.node.runAction(cc.sequence(cc.delayTime(2), callRevive));
	}

	//汽水搖完噴射
	ejection(Count: number) {
		// cc.log(Count);
		if (!this.reviving) {
			let dis = Count * 40;
			let time = dis / 1000;

			this.ejecting = true;
			this.jumping = false;
			this.falling = false;
			this.gathering = false;
			this.canJump = false;
			this.onGround = true;
			this.onIce = false;
			this.onFire = false;
			this.onHeal = false;

			this.getComponent(cc.RigidBody).active = false;

			let gather = cc.callFunc(() => {
				this.ejectingParam = 1;
			}, this);
			let ready = cc.spawn(cc.delayTime(1), gather);

			let ejectFunc = cc.callFunc(() => {
				this.ejectingParam = 2;
				this.schedule(
					() => {
						cc.find('Canvas/UI/Score/number').getComponent('score').add(3);
					},
					0.01,
					time / 0.01
				);
			}, this);
			let eject = cc.spawn(cc.moveBy(time, cc.v2(0, dis)), ejectFunc);

			let canMove = cc.callFunc(() => {
				this.ejectingParam = 3;
				this.ejecting = false;
			});
			let slow = cc.spawn(cc.moveBy(1.5, cc.v2(0, 200)), canMove);

			let ret = cc.callFunc(() => {
				this.ejectingParam = 0;
				this.getComponent(cc.RigidBody).active = true;

				this.jumping = true;
				this.falling = true;

				this.node.scaleX *= 0.8;
				this.drop();
			}, this);

			this.node.runAction(cc.sequence(ready, eject, slow, ret));
		}
	}

	updateAnimation() {
		if (this.isHost) {
			if (this.isDead) {
				if (this.animationState.name != 'Land') this.animationState = this.animator.play('Land');
			} else if (this.reviving) {
				this.animationState = this.animator.play();
			} else if (this.ejectingParam != 0) {
				if (this.ejectingParam == 1) {
					if (this.animationState.name != 'Gathering') {
						this.animationState = this.animator.play('Gathering');
						this.node.scaleX *= 1.25;
					}
				} else {
					if (this.animationState.name != 'eject') {
						this.animationState = this.animator.play('eject');
					}
				}
			} else if (this.falling) {
				if (this.animationState.name != 'Land') this.animationState = this.animator.play('Land');
			} else if (this.jumping) {
				if (this.animationState.name != 'Jump') {
					this.animationState = this.animator.play('Jump');
					this.node.scaleX *= 0.8;
				}
			} else if (this.gathering) {
				if (this.animationState.name != 'Gathering') {
					this.animationState = this.animator.play('Gathering');
					this.node.scaleX *= 1.25;
				}
			} else if (this.Speed != 0) {
				if (this.animationState.name != 'Run') this.animationState = this.animator.play('Run');
			} else this.animationState = this.animator.play();
		} else {
			if (this.remoteAniState != this.animationState.name) {
				this.animationState = this.animator.play(this.remoteAniState);
			}
		}
	}

	update(dt) {
		if (this.isHost != undefined) {
			if (cc.find('Canvas').getComponent('GameMgr').startSignal) {
				let camera = cc.find('Canvas/Main Camera');

				if (camera.y - this.node.y > 350 && !this.reviving) {
					if (cc.find('Canvas').getComponent('GameMgr').FHP(-70)) this.revive();
					if (!this.isDead) cc.audioEngine.playEffect(this.pongEffect, false);
				}

				this.updateAnimation();

				this.updateTrigger();

				//* check if host.
				if (this.isHost) {
					this.updateSpeed();

					if (!(this.jumpLock && !this.onGround)) {
						if (!this.gathering && !this.falling && !this.isDead && !this.ejecting) this.Move(dt);
					}
				}
			}
		} else this.isHost = cc.find('Canvas').getComponent('connect').isHost;
	}

	onKeyDown(evt) {
		let key = cc.macro.KEY;

		//* check if host.
		if (this.isHost) {
			switch (evt.keyCode) {
				case key.left:
					this.leftDown = true;
					break;
				case key.right:
					this.rightDown = true;
					break;
				case key.space:
					this.spaceDown = true;
					break;
				case key.down:
					this.downDown = true;
					break;
			}
		}
	}

	onKeyUp(evt) {
		let key = cc.macro.KEY;

		//* check if host.
		if (this.isHost) {
			switch (evt.keyCode) {
				case key.left:
					this.leftDown = false;
					break;
				case key.right:
					this.rightDown = false;
					break;
				case key.space:
					this.spaceDown = false;
					break;
				case key.down:
					this.downDown = false;
					break;
				case key.p:
					this.protect = !this.protect;
					cc.audioEngine.playEffect(this.click, false);
					cc.find('Canvas/UI/FallingProtect').getComponent(cc.Label).string = 'FallingProtect: ' + (this.protect ? 'ON' : 'OFF');
					cc.find('Canvas/UI/FallingProtect').color = this.protect ? cc.color(170, 30, 30) : cc.color(0, 0, 0);
					break;
				case key.l:
					this.jumpLock = !this.jumpLock;
					cc.audioEngine.playEffect(this.click, false);
					cc.find('Canvas/UI/JumpLock').getComponent(cc.Label).string = 'Jump Lock: ' + (this.jumpLock ? 'ON' : 'OFF');
					cc.find('Canvas/UI/JumpLock').color = this.jumpLock ? cc.color(170, 30, 30) : cc.color(0, 0, 0);
					break;
			}
		}
	}

	drop() {
		//* check if host.
		if (this.isHost && this.jumping) {
			cc.audioEngine.playEffect(this.fallEffect, false);
			this.breakBlock = true;

			this.getComponent(cc.RigidBody).type = cc.RigidBodyType.Static;

			let callback = cc.callFunc(() => {
				this.getComponent(cc.RigidBody).type = cc.RigidBodyType.Dynamic;
				this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, -800);
				this.falling = true;
			}, this);

			this.node.runAction(cc.sequence(cc.moveBy(0.2, cc.v2(0, 15)), callback));
		}
	}

	Move(dt) {
		let v = this.getComponent(cc.RigidBody).linearVelocity;

		if (v.y < -2) {
			//* falling
			this.onIce = false;
			this.onFire = false;
			this.onHeal = false;
		}

		let slow = cc.find('Canvas').getComponent('GameMgr').thirsty ? 0.7 : 1;

		if (this.onIce) {
			this.getComponent(cc.RigidBody).linearVelocity = cc.v2(v.x + this.Speed * dt * 1.5, v.y);
			this.node.x += this.Speed * dt * 0.3;
		} else this.node.x += this.Speed * dt * slow;

		if (this.reviving || this.ejectingParam == 3) {
			if (this.node.x < -515) this.node.x = -515;
			if (this.node.x > 145) this.node.x = 145;

			// this.node.y += cc.find('Canvas').getComponent('GameMgr').cameraSpeed;
		}
	}

	updateSpeed() {
		if (this.leftDown != this.rightDown) {
			this.Speed = this.leftDown ? -170 : 170;
		} else this.Speed = 0;

		if (!this.jumpLock) this.Speed *= 1.8;

		if (cc.find('Canvas').getComponent('GameMgr').Water < 30) this.Speed *= 0.7;

		if (this.getComponent(cc.RigidBody).linearVelocity.x != 0 && this.jumping)
			this.node.scaleX = this.getComponent(cc.RigidBody).linearVelocity.x > 0.1 ? -0.8 : 0.8;
		else this.node.scaleX = this.Speed > 0 ? -0.8 : 0.8;
	}

	onBeginContact(contact, selfCollider, otherCollider) {
		// TODO: remove protect if needed.
		if (otherCollider.node.group == 'blocks' || otherCollider.node.group == 'protect') {
			if (otherCollider.node.group == 'protect' && !this.protect) contact.disabled = true;
			else {
				if (contact.getWorldManifold().normal.y < -0.99) {
					//* land
					let v = this.getComponent(cc.RigidBody).linearVelocity;

					if (v.y < 100) {
						if (this.breakBlock && otherCollider.node.group == 'blocks' && otherCollider.node.name != 'Sand') {
							if (otherCollider.getComponent('brick')) otherCollider.getComponent('brick').break();
							else otherCollider.getComponent('staticBlock').break();
						}
						this.breakBlock = false;

						this.unschedule(this.preventJumpFail);

						if (otherCollider.node.name.slice(0, 3) == 'ice') {
							//* ice block

							this.getComponent(cc.RigidBody).linearVelocity = cc.v2(v.x * 0.9, v.y);

							this.onIce = true;
						} else {
							this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, v.y);
						}

						if (otherCollider.node.name.slice(0, 3) == 'fir') {
							this.onFire = true;
						} else if (otherCollider.node.name.slice(0, 3) == 'hea') {
							this.onHeal = true;
						}

						this.scheduleOnce(() => {
							this.onGround = true;
							this.jumping = false;
							this.falling = false;
						}, 0.1);
					}
				} else if (contact.getWorldManifold().normal.y > 0.99) {
					//* hit roof

					this.getComponent(cc.RigidBody).linearVelocity = cc.v2(
						this.getComponent(cc.RigidBody).linearVelocity.x,
						this.getComponent(cc.RigidBody).linearVelocity.y * -0.15
					);
				} else if (this.getComponent(cc.RigidBody).linearVelocity.x * contact.getWorldManifold().normal.x > 0) {
					//* hit wall

					this.getComponent(cc.RigidBody).linearVelocity = cc.v2(
						this.getComponent(cc.RigidBody).linearVelocity.x * -0.6,
						this.getComponent(cc.RigidBody).linearVelocity.y
					);
				}
			}
		}
	}
}
