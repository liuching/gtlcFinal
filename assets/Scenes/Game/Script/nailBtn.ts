// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class nailBtn extends cc.Component {
	// LIFE-CYCLE CALLBACKS:

	public Using = false;
	public nailCnt = 0;

	@property(cc.Prefab)
	icon: cc.Prefab = null;

	switch() {
		if (cc.find('Canvas').getComponent('connect').isHost) {
			if (this.nailCnt != 0) {
				this.Using = true;
				this.scheduleOnce(this.close, 5);

				this.node.scaleX = 1.15;
				this.node.scaleY = 1.15;
			}
		}
	}

	close() {
		this.Using = false;

		this.node.scaleX = 1;
		this.node.scaleY = 1;
	}

	// onLoad () {}

	start() {}

	// update (dt) {}
}
