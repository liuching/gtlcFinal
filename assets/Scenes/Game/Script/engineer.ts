// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class engineer extends cc.Component {
	@property(cc.Prefab)
	private brick: cc.Prefab[] = [];

	@property(cc.Prefab)
	private sign: cc.Prefab[] = [];

	public order_li = []; //用來記錄接下去的三個方塊 利用數字來對照接下來要產生的方塊跟圖案 0是接下去第一個 以此類推

	public LKey = cc.macro.KEY.left;
	public RKey = cc.macro.KEY.right;
	public DownKey = cc.macro.KEY.space;
	public LRotateKey = cc.macro.KEY.z;
	public RRotateKey = cc.macro.KEY.x;

	public leftDown: boolean = false;
	public rightDown: boolean = false;
	public zDown: boolean = false;
	public xDown: boolean = false;
	public spaceDown: boolean = false;

	public initSignal = true;

	public controlling = true;

	public isHost;

	onKeyDown(event) {
		if (!this.isHost) {
			if (event.keyCode == this.LRotateKey) {
				this.zDown = true;
			} else if (event.keyCode == this.RRotateKey) {
				this.xDown = true;
			} else if (event.keyCode == this.DownKey) {
				this.spaceDown = true;
			} else if (event.keyCode == this.LKey) {
				/* else if (event.keyCode == cc.macro.KEY.shift) {} */
				this.leftDown = true;
			} else if (event.keyCode == this.RKey) {
				this.rightDown = true;
			}
		}
	}

	onKeyUp(event) {
		if (!this.isHost) {
			if (event.keyCode == this.LRotateKey) {
				this.zDown = false;
			} else if (event.keyCode == this.RRotateKey) {
				this.xDown = false;
			} else if (event.keyCode == this.DownKey) {
				this.spaceDown = false;
			} else if (event.keyCode == this.LKey) {
				/*else if (event.keyCode == cc.macro.KEY.shift) {}*/
				this.leftDown = false;
			} else if (event.keyCode == this.RKey) {
				this.rightDown = false;
			}
		}
	}
	// LIFE-CYCLE CALLBACKS:

	DSend(target: string) {
		//* TODO;
		if (target == 'Brick') {
			let root = cc.find('Brick_Container', this.node);

			let ret = [];
			for (let i = 0; i < 10; i++) {
				let allChildren = root.getChildByName(String(i)).children;

				let temp = [];
				for (let i = 0; i < allChildren.length; i++)
					temp.push({
						position: allChildren[i].position,
						velocity: allChildren[i].getComponent(cc.RigidBody).linearVelocity,
						angle: allChildren[i].angle,
						gravity: allChildren[i].getComponent(cc.RigidBody).gravityScale,
						fixedRotation: allChildren[i].getComponent(cc.RigidBody).fixedRotation,
					});

				ret.push(temp);
			}

			return ret;
		} else if (target == 'Control') {
			return [this.leftDown, this.rightDown, this.spaceDown, this.zDown, this.xDown];
		} else if (target == 'BlockSign') {
			return this.order_li;
		}
	}

	DGet(src, target: string) {
		if (target == 'Brick') {
			let root = cc.find('Brick_Container', this.node);

			for (let i = 0; i < 10; i++) {
				let Count = root.getChildByName(String(i)).childrenCount;
				let allChildren = root.getChildByName(String(i)).children;

				if (src && src[i] != undefined) {
					for (let k = 0; k < src[i].length; k++) {
						if (k < Count) {
							allChildren[k].position = src[i][k].position;
							allChildren[k].angle = src[i][k].angle;
						} else {
							let brick = cc.instantiate(this.brick[i]);

							brick.getComponent('brick').init(cc.find('Brick_Container', this.node), i, false);

							brick.position = src[i][k].position;
							brick.getComponent(cc.RigidBody).linearVelocity = src[i][k].velocity;
							brick.angle = src[i][k].angle;
							brick.getComponent(cc.RigidBody).gravityScale = src[i][k].gravity;
							brick.getComponent(cc.RigidBody).fixedRotation = src[i][k].fixedRotation;
						}
					}

					for (let k = src[i].length; k < Count; k++) root.getChildByName(String(i)).removeChild(allChildren[k]);
				}
			}
		} else if (target == 'Control') {
			if (src) {
				this.leftDown = src[0];
				this.rightDown = src[1];
				this.spaceDown = src[2];
				this.zDown = src[3];
				this.xDown = src[4];
			}
		} else if (target == 'BlockSign') {
			if (src) this.setArr(src[0], src[1], src[2]);
		}
	}

	onLoad() {
		cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
		cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
	}

	start() { }

	update() {
		if (this.isHost != undefined) {
			if (cc.find('Canvas').getComponent('GameMgr').startSignal) {
				if (this.initSignal && this.isHost != undefined) {
					if (this.isHost) {
						for (let i = 0; i < 3; i++) this.order_li[i] = Math.floor(Math.random() * 10);
						this.createBrick();
					}

					this.initSignal = false;
				}
			}
		} else this.isHost = this.getComponent('connect').isHost;
	}

	//控制方塊

	//setArr是顯示右上方接下來三個控制方塊
	setArr(a, b, c) {
		cc.find('nextBrick_Container', this.node).removeAllChildren();
		// cc.log('clean');
		this.order_li[0] = a;
		this.order_li[1] = b;
		this.order_li[2] = c;
		let sign1, sign2, sign3;

		if (this.order_li[0] != 10) {
			sign1 = cc.instantiate(this.sign[this.order_li[0]]);
			sign1.getComponent('sign').init(cc.find('nextBrick_Container', this.node), 1); //產生接下來第一個方塊 設定位置
		}
		if (this.order_li[1] != 10) {
			sign2 = cc.instantiate(this.sign[this.order_li[1]]);
			sign2.getComponent('sign').init(cc.find('nextBrick_Container', this.node), 2); //產生接下來第二個方塊 設定位置
		}
		if (this.order_li[2] != 10) {
			sign3 = cc.instantiate(this.sign[this.order_li[2]]);
			sign3.getComponent('sign').init(cc.find('nextBrick_Container', this.node), 3); //產生接下來第三個方塊 設定位置
		}
	}

	//產生下一個方塊
	createBrick() {
		let ra = Math.floor(Math.random() * 10);
		if (cc.find('Canvas').getComponent('GameMgr').curScene == 2)
			ra = 10;

		if (this.order_li[0] == 10)
			this.controlling = false;
		else {
			let brick = cc.instantiate(this.brick[this.order_li[0]]);

			brick.getComponent('brick').init(cc.find('Brick_Container', this.node), this.order_li[0], true); //設定下一個落下方塊的位置
			this.setArr(this.order_li[1], this.order_li[2], ra); //把接下來的三個方塊ID丟到setarr function來產生圖示
		}
	}
	//控制方塊在碰到地面或方塊之後要產生下一個方塊

	// TODO
	//方塊左右移動(按住移動一次) 右邊方塊顯示 旋轉 方塊生成
}
