// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class countdown extends cc.Component {
	// LIFE-CYCLE CALLBACKS:

	onLoad() {
		this.schedule(
			() => {
				let num = Number(this.node.getChildByName('number').getComponent(cc.Label).string);

				if (num == 0) {
					{
						cc.find('Canvas').getComponent('GameMgr').startSignal = true;
						this.node.runAction(cc.hide());
					}
				} else this.node.getChildByName('number').getComponent(cc.Label).string = String(num - 1);
			},
			1,
			5
		);
	}

	start() {}

	// update (dt) {}
}
