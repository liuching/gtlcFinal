// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
declare var firebase: any;
const { ccclass, property } = cc._decorator;

@ccclass
export default class ReadyEngine extends cc.Component {
	public user;
	public room;
	// LIFE-CYCLE CALLBACKS:

	public ret;
	@property(cc.AudioClip)
	click: cc.AudioClip = null;
	@property(cc.AudioClip)
	bgm: cc.AudioClip = null;

	onLoad() {
		if (!cc.audioEngine.isMusicPlaying()) cc.audioEngine.playMusic(this.bgm, true);
		let database = firebase.database();

		if (!cc.find('remote')) cc.director.loadScene('RoomList');

		firebase.auth().onAuthStateChanged(() => {
			this.user = firebase.auth().currentUser;

			if (this.user) {
				database.ref('player/' + this.user.uid).once('value', item => {
					if (item.val()) {
						this.room = item.val();

						let ret = database.ref('room/' + this.room).on('value', snapshot => {
							//* handle start button.
							//! ignore console error.
							//?
							if (!snapshot.val().player[0] || snapshot.val().player[0].name != this.user.uid)
								cc.find('start').runAction(cc.hide());
							else cc.find('start').runAction(cc.show());

							let AllReady = true;
							for (let i = 0; i < snapshot.val().max; i++)
								if (!snapshot.val().player[i] || !snapshot.val().player[i].ready) AllReady = false;

							if (AllReady) cc.find('start').getComponent(cc.Button).interactable = true;
							else cc.find('start').getComponent(cc.Button).interactable = false;

							//* room name.
							cc.find('roomname').getComponent(cc.EditBox).string = snapshot.val().name;

							//* player and ready
							for (let i = 0; i < 2; i++) {
								if (snapshot.val().player[i]) {
									cc.find(i + 'Cut').active = true;

									//* name
									let uid = snapshot.val().player[i].name;

									database.ref('username/' + uid).once('value', data => {
										cc.find('player' + i).getComponent(cc.Label).string = data.val();
									});

									//* ready
									cc.find('yes' + i).active = snapshot.val().player[i].ready;
								} else {
									cc.find(i + 'Cut').active = false;
									cc.find('player' + i).getComponent(cc.Label).string = '';
									cc.find('yes' + i).active = false;
								}
							}
						});

						cc.find('remote')
							.getComponent('remote')
							.add('room/' + this.room, 'value', ret);

						ret = database.ref('room/' + this.room + '/status').on('value', data => {
							if (data.val() == 'playing') {
								cc.find('remote').getComponent('remote').clear();

								this.scheduleOnce(() => {
									cc.director.loadScene('Load');
								}, 3);
							}
						});

						cc.find('remote')
							.getComponent('remote')
							.add('room/' + this.room + '/status', 'value', ret);
					} else cc.director.loadScene('RoomList');
				});
			} else cc.director.loadScene('Login');
		});
	}

	start() { }

	ready() {
		cc.audioEngine.playEffect(this.click, false);
		let database = firebase.database();

		let i, cur;
		//* ready button.
		database.ref('room/' + this.room).once('value', snapshot => {
			for (i = 0; i < snapshot.val().max; i++)
				if (snapshot.val().player[i] && snapshot.val().player[i].name == this.user.uid) break;

			database.ref('room/' + this.room + '/player/' + i + '/ready').once('value', item => {
				cur = item.val();

				database.ref('room/' + this.room + '/player/' + i + '/ready').set(!cur);
			});
		});
	}

	quit() {
		cc.audioEngine.playEffect(this.click, false);
		let database = firebase.database();

		let us;
		let count = 0;
		//* handle leaving room.
		database.ref('room/' + this.room).once('value', snapshot => {
			for (let i = 0; i < snapshot.val().max; i++) {
				if (snapshot.val().player[i] && snapshot.val().player[i].name == this.user.uid) us = i;
				if (snapshot.val().player[i]) count++;
			}

			if (count == 1) database.ref('room').child(this.room).remove();
			else database.ref('room/' + this.room + '/player/' + us).remove();

			database.ref('player').child(this.user.uid).remove();

			cc.find('remote').getComponent('remote').clear();
			cc.director.loadScene('RoomList');
		});
	}

	updateRoomName() {
		cc.audioEngine.playEffect(this.click, false);
		let database = firebase.database();
		database.ref('room/' + this.room + '/name').set(cc.find('roomname').getComponent(cc.EditBox).string);
	}

	startGame() {
		cc.audioEngine.playEffect(this.click, false);
		let database = firebase.database();
		database.ref('room/' + this.room + '/status').set('playing');
	}
	// update (dt) {}
}
