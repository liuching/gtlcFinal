// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
declare var firebase: any;
const { ccclass, property } = cc._decorator;

@ccclass
export default class MatchEngine extends cc.Component {
	// LIFE-CYCLE CALLBACKS:

	public user;

	@property(cc.Prefab)
	room: cc.Prefab = null;
	@property(cc.AudioClip)
	click: cc.AudioClip = null;
	@property(cc.AudioClip)
	bgm: cc.AudioClip = null;

	//* height info(original + 5).
	public roomHeight = 70;

	onLoad() {
		if (!cc.audioEngine.isMusicPlaying()) cc.audioEngine.playMusic(this.bgm, true);
		firebase.auth().onAuthStateChanged(() => {
			if (!firebase.auth().currentUser) cc.director.loadScene('Login');
			else {
				this.user = firebase.auth().currentUser;
				let database = firebase.database();

				database.ref('player').once('value', snapshot => {
					if (snapshot.hasChild(this.user.uid)) {
						if (snapshot.val()[this.user.uid] == 'local') cc.director.loadScene('Load');
						else cc.director.loadScene('inRoom');
					} else {
						//* room list add.
						let ret = database.ref('room').on('child_added', snapshot => {
							cc.find('Canvas/scroll/view/content').height += this.roomHeight;
							cc.find('Canvas/scroll/view/content/layout').height += this.roomHeight;

							//! ignore console error.
							let room = cc.instantiate(this.room);
							room.getComponent('RoomJoin').init(snapshot.key);
						});

						//* add to remote list.
						cc.find('remote').getComponent('remote').add('room', 'child_added', ret);

						//* room list remove.
						ret = database.ref('room').on('child_removed', snapshot => {
							var children = cc.find('Canvas/scroll/view/content/layout').children;

							for (let i = 0; i < children.length; i++) {
								if (children[i].getComponent('RoomJoin').roomName == snapshot.key) {
									children[i].destroy();

									cc.find('Canvas/scroll/view/content').height -= this.roomHeight;
									cc.find('Canvas/scroll/view/content/layout').height -= this.roomHeight;
								}
							}
						});

						//* add to remote list.
						cc.find('remote').getComponent('remote').add('room', 'child_removed', ret);
					}
				});
			}
		});
	}

	start() { }

	createRoom() {
		cc.audioEngine.playEffect(this.click, false);
		let database = firebase.database();

		//* room random number.
		let num = Math.floor(Math.random() * 10000);

		let newRoom = database.ref('room').push({
			name: 'ROOM ' + num,
			max: 2,
			status: 'waiting',
			player: {
				0: { name: this.user.uid, ready: false },
			},
		});

		database.ref('player').child(this.user.uid).set(newRoom.key);

		//* clear all remote.
		cc.find('remote').getComponent('remote').clear();
		this.scheduleOnce(() => {
			cc.director.loadScene('inRoom');
		}, 0.5);
	}

	logOut() {
		cc.audioEngine.playEffect(this.click, false);
		firebase.auth().signOut();
	}

	local() {
		cc.audioEngine.playEffect(this.click, false);
		let database = firebase.database();

		database.ref('player').child(this.user.uid).set('local');

		//* clear all remote.
		cc.find('remote').getComponent('remote').clear();
		this.scheduleOnce(() => {
			cc.director.loadScene('Load');
		}, 0.5);
	}
	// update (dt) {}
}
