// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
declare var firebase: any;
const { ccclass, property } = cc._decorator;

@ccclass
export default class RoomJoin extends cc.Component {
	// LIFE-CYCLE CALLBACKS:

	public user;
	public roomName;

	@property(cc.SpriteFrame)
	normal: cc.SpriteFrame = null;

	@property(cc.SpriteFrame)
	hover: cc.SpriteFrame = null;

	@property(cc.SpriteFrame)
	full: cc.SpriteFrame = null;

	@property(cc.Label)
	nameLabel: cc.Label = null;

	init(src: string) {
		let database = firebase.database();

		this.nameLabel.string = src;
		this.roomName = src;

		this.node.parent = cc.find('Canvas/scroll/view/content/layout');

		//* add click event.
		let Act = new cc.Component.EventHandler();

		Act.target = this.node;
		Act.component = 'RoomJoin';
		Act.handler = 'active';
		Act.customEventData = src;

		this.getComponent(cc.Button).clickEvents.push(Act);

		let ret = database.ref('room/' + this.roomName).on('value', snapshot => {
			if (snapshot.val()) {
				if (snapshot.val().status != 'playing') {
					this.getComponent(cc.Button).interactable = true;

					//* Owner
					for (let i = 0; i < snapshot.val().max; i++)
						if (snapshot.val().player[i]) {
							database.ref('username/' + snapshot.val().player[i].name).once('value', snapshot => {
								cc.find('Background/Owner', this.node).getComponent(cc.Label).string = snapshot.val();
							});
						}

					//* roomName.
					cc.find('Background/Roomname', this.node).getComponent(cc.Label).string = snapshot.val().name;

					//* member.
					cc.find('Background/Number', this.node).getComponent(cc.Label).string =
						Object.keys(snapshot.val().player).length + '/' + snapshot.val().max;

					//* room icon.
					if (Object.keys(snapshot.val().player).length == snapshot.val().max) {
						database.ref('room/' + snapshot.key + '/status').set('full');
						this.getComponent(cc.Button).hoverSprite = this.full;
						this.getComponent(cc.Button).normalSprite = this.full;
						this.getComponent(cc.Button).pressedSprite = this.full;
					} else {
						database.ref('room/' + snapshot.key + '/status').set('waiting');
						this.getComponent(cc.Button).normalSprite = this.normal;
						this.getComponent(cc.Button).hoverSprite = this.hover;
						this.getComponent(cc.Button).pressedSprite = this.normal;
					}
				} else this.getComponent(cc.Button).interactable = false;
			}
		});

		//* add to remote list.
		cc.find('remote')
			.getComponent('remote')
			.add('room/' + this.roomName, 'value', ret);
	}

	active(event, src) {
		let database = firebase.database();

		//* check if room is full.
		database.ref('room/' + src).once('value', snapshot => {
			for (let i = 0; i < snapshot.val().max; i++) {
				if (!snapshot.val().player[i]) {
					database.ref('room/' + src + '/player/' + i).set({ name: this.user.uid, ready: false });
					database.ref('player').child(this.user.uid).set(src);

					//* clear all remote.
					cc.find('remote').getComponent('remote').clear();
					cc.director.loadScene('inRoom');
					break;
				}
			}
		});
	}

	onLoad() {
		firebase.auth().onAuthStateChanged(() => {
			this.user = firebase.auth().currentUser;
		});
	}

	start() {}

	// update (dt) {}
}
