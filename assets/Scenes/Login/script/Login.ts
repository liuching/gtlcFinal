// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
declare var firebase: any;
const { ccclass, property } = cc._decorator;

@ccclass
export default class Login extends cc.Component {
	// LIFE-CYCLE CALLBACKS:
	@property({ type: cc.AudioClip })
	bgm: cc.AudioClip = null;
	@property({ type: cc.AudioClip })
	click: cc.AudioClip = null;
	private inclick = false;
	private upclick = false;

	onLoad() {
		firebase.auth().onAuthStateChanged(() => {
			if (firebase.auth().currentUser) cc.director.loadScene('RoomList');
		});
	}

	start() {
		let audioID = cc.audioEngine.playMusic(this.bgm, true);
		cc.audioEngine.setVolume(audioID, 0.5);
	}

	callSignUp() {
		cc.audioEngine.playEffect(this.click, false);
		if (!this.upclick) cc.find('Canvas/SignUpWindow').runAction(cc.moveBy(1, cc.v2(930, 0)).easing(cc.easeCubicActionInOut()));
		this.upclick = true;
	}

	callSignIn() {
		cc.audioEngine.playEffect(this.click, false);
		if (!this.inclick) cc.find('Canvas/LoginWindow').runAction(cc.moveBy(1, cc.v2(930, 0)).easing(cc.easeCubicActionInOut()));
		this.inclick = true;
	}

	cancelSignUp() {
		cc.audioEngine.playEffect(this.click, false);
		this.upclick = false;
		cc.find('Canvas/SignUpWindow').runAction(cc.moveBy(1, cc.v2(-930, 0)).easing(cc.easeCubicActionInOut()));
	}

	cancelSignIn() {
		cc.audioEngine.playEffect(this.click, false);
		this.inclick = false;
		cc.find('Canvas/LoginWindow').runAction(cc.moveBy(1, cc.v2(-930, 0)).easing(cc.easeCubicActionInOut()));
	}

	signUp() {
		//* need to set it.
		cc.audioEngine.playEffect(this.click, false);
		let email = cc.find('Canvas/SignUpWindow/email/input').getComponent(cc.Label).string;
		let password = cc.find('Canvas/SignUpWindow/password/input').getComponent(cc.Label).string;
		let username = cc.find('Canvas/SignUpWindow/username/input').getComponent(cc.Label).string;

		firebase
			.auth()
			.createUserWithEmailAndPassword(email, password)
			.then(result => {
				let database = firebase.database();

				database.ref('username/' + result.user.uid).set(username);
				cc.director.loadScene('RoomList');
			})
			.catch(error => {
				cc.find('Canvas/error').getComponent(cc.Label).string = error.code + ': ' + error.message;
			});
	}

	signIn() {
		//* need to set it.
		cc.audioEngine.playEffect(this.click, false);
		let email = cc.find('Canvas/LoginWindow/email/input').getComponent(cc.Label).string;
		let password = cc.find('Canvas/LoginWindow/password/input').getComponent(cc.Label).string;

		firebase
			.auth()
			.signInWithEmailAndPassword(email, password)
			.then(() => {
				cc.director.loadScene('RoomList');
			})
			.catch(error => {
				cc.find('Canvas/error').getComponent(cc.Label).string = error.code + ': ' + error.message;
			});
	}
	// update (dt) {}
}
