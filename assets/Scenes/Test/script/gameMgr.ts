// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
declare var firebase: any;
const { ccclass, property } = cc._decorator;

var user;
var host;

export { user, host };

@ccclass
export default class gameMgr extends cc.Component {
	// LIFE-CYCLE CALLBACKS:

	@property(cc.Node)
	player: cc.Node = null;

	updateUserLabel(data) {
		cc.find('Canvas/User').getComponent(cc.Label).string = 'User: ' + data.slice(0, 6);
	}

	updateHostLabel(data) {
		cc.find('Canvas/Host').getComponent(cc.Label).string = 'Host: ' + data.slice(0, 6);
	}

	onLoad() {
		let database = firebase.database();
		// update user and host.
		database.ref('host').on('value', data => {
			host = data.val();

			//* info.
			this.updateHostLabel(host);
		});

		firebase.auth().onAuthStateChanged(() => {
			user = firebase.auth().currentUser;

			//* info.
			if (user) this.updateUserLabel(user.uid);
			else this.updateUserLabel('NaN');
		});

		this.schedule(() => {
			let database = firebase.database();
			let data = this.getGameState();

			if (!user || user.uid != host) {
				database.ref('game').once('value', snapshot => {
					data = snapshot.val();
					this.updateGame(data);
				});
			} else {
				database.ref('game').set(data);
			}
		}, 0.016);
	}

	getGameState() {
		let position = this.player.position;
		let velocity = this.player.getComponent(cc.RigidBody).linearVelocity;

		return { position, velocity };
	}

	updateGame(data) {
		this.player.position = cc.v2(data.position.x, data.position.y);
		this.player.getComponent(cc.RigidBody).linearVelocity = cc.v2(data.velocity.x, data.velocity.y);
	}

	start() {}

	update(dt) {}

	signUp() {
		let email = cc.find('Canvas/email/input').getComponent(cc.Label).string;
		let password = cc.find('Canvas/password/input').getComponent(cc.Label).string;

		firebase
			.auth()
			.createUserWithEmailAndPassword(email, password)
			.then(result => {
				//* info.
				this.updateUserLabel(result.user.uid);
			});
	}

	signIn() {
		let email = cc.find('Canvas/email/input').getComponent(cc.Label).string;
		let password = cc.find('Canvas/password/input').getComponent(cc.Label).string;

		firebase
			.auth()
			.signInWithEmailAndPassword(email, password)
			.then(result => {
				//* info.
				this.updateUserLabel(result.user.uid);
			});
	}

	logOut() {
		firebase.auth().signOut();
	}

	changeHost() {
		let database = firebase.database();
		let user = firebase.auth().currentUser;

		database.ref('host').set(user.uid);

		//* info.
		this.updateUserLabel(user.uid);
	}
}
