// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
declare var firebase: any;
const { ccclass, property } = cc._decorator;
import { user, host } from './gameMgr';

@ccclass
export default class player extends cc.Component {
	// LIFE-CYCLE CALLBACKS:

	private speed: number = 0;
	private leftDown: boolean = false;
	private rightDown: boolean = false;

	onLoad() {
		cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
		cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);

		cc.director.getPhysicsManager().enabled = true;
	}

	onKeyDown(evt) {
		let macro = cc.macro;

		switch (evt.keyCode) {
			case macro.KEY.left:
				this.leftDown = true;
				break;
			case macro.KEY.right:
				this.rightDown = true;
				break;
			case macro.KEY.space:
				this.jump();
		}
	}

	onKeyUp(evt) {
		let macro = cc.macro;

		switch (evt.keyCode) {
			case macro.KEY.left:
				this.leftDown = false;
				break;
			case macro.KEY.right:
				this.rightDown = false;
				break;
		}
	}

	jump() {
		this.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 350);
	}

	start() {}

	update(dt) {
		if (user && host) {
			if (user.uid == host) {
				this.updateSpeed();
				this.Move(dt);
			}
		}
	}

	Move(dt) {
		this.node.x += this.speed * dt;
	}
	updateSpeed() {
		if (this.leftDown != this.rightDown) this.speed = this.leftDown ? -100 : 100;
		else this.speed = 0;
	}
}
